﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Wp2Android
{
    public class EntryPoint
    {
        public static Task<HttpResponseMessage> Run(HttpRequestMessage req, ILogger log, CancellationToken token)
        {
            return Bot.Run(req, log, token);
        }
    }
}
