﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using File = Telegram.Bot.Types.File;

namespace Wp2Android
{
    public static class Bot
    {
        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, ILogger log, CancellationToken token)
        {
            if (first)
            {
                token.Register(() =>
                {
                    Telemetry?.TrackEvent("TelemetryClientFlush");
                    Telemetry?.Flush();
                });
                first = false;
            }

            try
            {
                using (var operation = Telemetry?.StartOperation<RequestTelemetry>("BotCommunication"))
                {
                    Telemetry?.TrackEvent("Run");

                    var response = await RunInternal(req, log);

                    if (operation != null)
                        operation.Telemetry.ResponseCode = response.StatusCode.ToString();

                    Telemetry?.StopOperation(operation);
                    return response;
                }

            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                Bot.Telemetry?.TrackException(e);
                return req.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        private static async Task<HttpResponseMessage> RunInternal(HttpRequestMessage req, ILogger log)
        {
            var update = await req.Content.ReadAsAsync<Update>();

            if (Telemetry != null)
                Telemetry.Context.User.Id = update.Message?.From?.Id.ToString();

            var message = update.Message;

            if (message == null)
            {
                log.Info("Got no message");
                return req.CreateResponse(HttpStatusCode.BadRequest);
            }

            log.Info($"Got message: {message.MessageId}, type: {message.Type}, document: {message.Document != null}");

            if (message.Entities?.Any(e => e.Type == Telegram.Bot.Types.Enums.MessageEntityType.BotCommand) ?? false)
            {
                return req.CreateResponse(await HandleCommandAsync(message, log));
            }

            if (message.Document != null)
            {
                return req.CreateResponse(await HandleFileAsync(message, log));
            }

            return req.CreateResponse(await HandleTextAsync(message, log));
        }

        private static bool first;

        public static async Task<HttpStatusCode> HandleTextAsync(Message message, ILogger log)
        {
            await Api.SendTextMessageAsync(message.Chat.Id, @"Nice to see You. Now send me Your `sms.vmsg` file and I will convert it to xml.", parseMode: ParseMode.Markdown);
            Telemetry?.TrackEvent("SendText");
            return HttpStatusCode.OK;
        }

        public static async Task<HttpStatusCode> HandleCommandAsync(Message message, ILogger log)
        {
            var command = message.Entities.FirstOrDefault(e => e.Type == Telegram.Bot.Types.Enums.MessageEntityType.BotCommand);
            if (command == null)
            {
                return HttpStatusCode.NotFound;
            }

            if (message.Text == "/start")
            {
                await Api.SendTextMessageAsync(message.Chat.Id, @"Welcome to *Windows Phone to Android SMS convertor* bot.
Gimme your `sms.vmsg` file, I will return You xml file that compatible with `Sms Backup & Restore` android app.", parseMode: ParseMode.Markdown);
                await Api.SendTextMessageAsync(message.Chat.Id, @"For windows phone use application [Transfer my Data](https://www.microsoft.com/en-us/store/phoneappid/dc08943b-7b3d-4ee5-aa3c-30f1a826af02) to export sms.", parseMode: ParseMode.Markdown);
                await Api.SendTextMessageAsync(message.Chat.Id, @"For android phone use application [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore) to restore sms", parseMode: ParseMode.Markdown);

                Telemetry?.TrackEvent("UserWelcome");

                return HttpStatusCode.OK;
            }

            return HttpStatusCode.NotImplemented;
        }

        public static async Task<HttpStatusCode> HandleFileAsync(Message message, ILogger log)
        {
            File file;
            try
            {
                
                file = await Api.GetFileAsync(message.Document.FileId);
            }
            catch (Exception e)
            {
                log.Info(e.Message);
                Telemetry?.TrackException(e);
                await Api.SendTextMessageAsync(message.Chat.Id, "Due to Telegram Api limitations I cannot handle files with size more than 20Mb.",
                    replyToMessageId: message.MessageId);
                return HttpStatusCode.OK;
            }

            Telemetry?.TrackMetric("UploadedFileSize", file.FileStream.Length);
            var converter = new MessageConverter();

            try
            {
                log.Info("Converting");
                var sw = new Stopwatch();
                sw.Start();

                var ms = new MemoryStream();

                converter.Convert(file.FileStream, ms);
                Telemetry?.TrackMetric("ResutFileSize", ms.Length);
                ms.Seek(0, SeekOrigin.Begin);

                var fileToSend = new FileToSend($"smsbackup_{DateTime.UtcNow:yyyyMMddHHmmss}.xml", ms);

                log.Info("Sending response");

                if ((DateTime.UtcNow - message.Date).TotalSeconds > 5)
                {
                    await Api.SendTextMessageAsync(message.Chat.Id, "Ohhh, this took a while to process! Sorry for wait.");
                }
                if (ms.Length > 50*1024*1024)
                {
                    await
                        Api.SendTextMessageAsync(message.Chat.Id,
                            "I'm really sorry. But resulting xml is just too big for me.",
                            replyToMessageId: message.MessageId);
                }
                else
                {
                    await Api.SendDocumentAsync(message.Chat.Id, fileToSend,
                        replyToMessageId: message.MessageId,
                        caption: "You can now open this file with SMS Backup & Restore application");
                }

                Telemetry?.TrackMetric("ConvertionTime", sw.Elapsed.TotalMilliseconds);
            }
            catch (Exception e)
            {
                log.Info(e.Message);
                Telemetry?.TrackException(e);
                await Api.SendTextMessageAsync(message.Chat.Id, "There seems to be a problem with sms.vmsg file. =( Sorry for that");
            }

            return HttpStatusCode.OK;
        }

        public static ITelegramBotClient Api => new TelegramBotClient(ConfigurationManager.AppSettings["BotApiKey"]);

        public static readonly TelemetryClient Telemetry = string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["InstrumentationKey"]) ? null : new TelemetryClient
        {
            InstrumentationKey = ConfigurationManager.AppSettings["InstrumentationKey"]
        };
    }
}
