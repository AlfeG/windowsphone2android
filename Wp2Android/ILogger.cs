﻿using System;

namespace Wp2Android
{
    public interface ILogger
    {
        void Info(string message, string component = null);
        void Error(string message, Exception exception = null);
    }
}