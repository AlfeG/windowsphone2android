﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace Wp2Android
{
    public class MessageConverter
    {
        // ReSharper disable InconsistentNaming
        const string BEGIN_VMSG = "BEGIN:VMSG";
        const string END_VMSG = "END:VMSG";
        const string TEL = "TEL:";
        const string DATE = "Date:";
        const string SUBJECT = "Subject;";
        const string SUBJECT_PREAMBULA = "ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:";
        const string SENDBOX = "X-BOX:SENDBOX";
        const string INBOX = "X-BOX:INBOX";
        // ReSharper restore InconsistentNaming

        public void Convert(Stream vmsg, Stream output)
        {
            var sw = new StreamWriter(output);
            var smses = Parse(vmsg).Distinct().ToList();

            sw.WriteLine("<?xml version=\'1.0\' encoding=\'UTF-8\' standalone=\'yes\' ?>");
            sw.WriteLine($"<smses count='{smses.Count}'>");

            Bot.Telemetry?.TrackMetric("SmsCount", smses.Count);

            foreach (var sms in smses)
            {
                sw.WriteLine(" " + ToSmsBackupItem(sms));
            }

            sw.WriteLine("</smses>");
            sw.Flush();
        }

        string ToSmsBackupItem(SmsMessage sms)
        {
            var xml = new XElement(XName.Get("sms", ""));
            xml.Add(new XAttribute("protocol", "0"));
            xml.Add(new XAttribute("address", RemoveInvalidXmlChars(sms.Tel)));
            xml.Add(new XAttribute("date", new DateTimeOffset(sms.Date).ToUnixTimeMilliseconds()));
            xml.Add(new XAttribute("type", (int)sms.Type));
            xml.Add(new XAttribute("subject", "null"));
            xml.Add(new XAttribute("body", RemoveInvalidXmlChars(sms.Content)));
            xml.Add(new XAttribute("toa", "null"));
            xml.Add(new XAttribute("sc_toa", "null"));
            xml.Add(new XAttribute("service_center", "null"));
            xml.Add(new XAttribute("read", "1"));
            xml.Add(new XAttribute("status", "-1"));
            xml.Add(new XAttribute("locked", "0"));
            return xml.ToString(SaveOptions.DisableFormatting);
        }

        string RemoveInvalidXmlChars(string text)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            var length = text.Length;
            var stringBuilder = new StringBuilder(length);

            for (var i = 0; i < length; ++i)
            {
                if (XmlConvert.IsXmlChar(text[i]))
                {
                    stringBuilder.Append(text[i]);
                }
                else if (i + 1 < length && XmlConvert.IsXmlSurrogatePair(text[i + 1], text[i]))
                {
                    stringBuilder.Append(text[i]);
                    stringBuilder.Append(text[i + 1]);
                    ++i;
                }
            }

            return stringBuilder.ToString();
        }

        IEnumerable<SmsMessage> Parse(Stream vmsg)
        {
            SmsMessage sms = null;
            using (var sr = new StreamReader(vmsg))
            {

                string line;
                var subject = "";
                var readingBody = false;

                while ((line = sr.ReadLine()) != null)
                {
                    if (line.StartsWith(BEGIN_VMSG, StringComparison.Ordinal))
                    {
                        sms = new SmsMessage();
                        continue;
                    }

                    if (line.StartsWith(END_VMSG, StringComparison.Ordinal))
                    {
                        if (sms == null) continue;

                        sms.Content = VCardReader.VCardReader.DecodeQuotedPrintable(subject.Substring(SUBJECT_PREAMBULA.Length), Encoding.UTF8);
                        subject = string.Empty;
                        readingBody = false;
                        yield return sms;
                        continue;
                    }

                    if (line.StartsWith(TEL, StringComparison.Ordinal))
                    {
                        if (sms == null) continue;
                        sms.Tel = line.Substring(TEL.Length);
                        continue;
                    }

                    if (line.StartsWith(DATE, StringComparison.Ordinal))
                    {
                        if (sms == null) continue;
                        sms.Date = DateTime.Parse(line.Substring(DATE.Length));
                        continue;
                    }

                    if (line == INBOX)
                    {
                        if (sms == null) continue;
                        sms.Type = SmsType.Inbox;
                        continue;
                    }

                    if (line == SENDBOX)
                    {
                        if (sms == null) continue;
                        sms.Type = SmsType.Sendbox;
                        continue;
                    }

                    if (line.StartsWith(SUBJECT, StringComparison.Ordinal))
                    {
                        subject += line.Substring(SUBJECT.Length).TrimEnd('=');
                        readingBody = line.EndsWith("=", StringComparison.Ordinal);
                        continue;
                    }

                    if (readingBody)
                    {
                        subject += line.TrimEnd('=');
                        readingBody = line.EndsWith("=", StringComparison.Ordinal);
                        continue;
                    }
                }
            }
        }

        enum SmsType { Inbox = 1, Sendbox = 2 }

        class SmsMessage
        {
            bool Equals(SmsMessage other)
            {
                return string.Equals(Tel, other.Tel) && Date.Equals(other.Date) && string.Equals(Content, other.Content) && Type == other.Type;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != GetType()) return false;
                return Equals((SmsMessage)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = Tel?.GetHashCode() ?? 0;
                    hashCode = (hashCode * 397) ^ Date.GetHashCode();
                    hashCode = (hashCode * 397) ^ (Content?.GetHashCode() ?? 0);
                    hashCode = (hashCode * 397) ^ (int)Type;
                    return hashCode;
                }
            }

            public string Tel { get; set; }
            public DateTime Date { get; set; }
            public string Content { get; set; }
            public SmsType Type { get; set; }
        }
    }
}
