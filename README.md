# Windows Phone 2 Android Bot #

This is a source of a bot for `Telegram`, that will help transfer all `Windows Phone` SMS messages to `Android`

## Bot Offline

Please use compiled version https://bitbucket.org/AlfeG/windowsphone2android/downloads/Wp2Android.Runner.exe (or compile from master)

`Wp2Android.Runner.exe <path to sms.vmsg>`

### Russian

Телеграм бот не работает. Пожалуйста, используйте оффлайн версию конвертора  https://bitbucket.org/AlfeG/windowsphone2android/downloads/Wp2Android.Runner.exe 

Запускать надо в коммандной строке или Powershell

`Wp2Android.Runner.exe <path to sms.vmsg>`

Рядом с файлом указанным в аргументе будет второй, который нужно скормить Sms backup приложению

### What is this repository for? ###

To share code of the bot.

To make my life easier.

I don't want to be asked - is my messages will be stored somewhere? (Yes, on telegram servers, you can delete them)

### How do I get set up? ###

Just run `deploy.cmd` file. It will fire up Cake build script that will build project.

If there is a need to deploy Your own bot, then things is a bit trickier.

You will need to read about bots at Telegram docs. Speak with http://telegram.me/BotFather to create you own bot.

You will need Azure account. Create `Azure Function App` resource. And upload `Functions\Bot` folder content to You function location.

`App Settings` should contain Your `bot token` - `BotApiKey` setting and optionally `ApplicationInsights` `InstrumentationKey` setting

### Contacts ###

* http://telegram.me/alfeg
* http://twitter.com/alfeg