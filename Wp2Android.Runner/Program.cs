﻿using System;
using System.IO;
using Telegram.Bot;

namespace Wp2Android.Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                if (args.Length > 0 && args[0] == "hook")
                {
                    HookBot(args);
                    return;
                }

                Console.WriteLine(@"Wp2Android.Runner

Wp2Android.Runner.exe <path to sms.vmsg>

will convert sms.vmsg to xml compatible with SMS Backup & Restore


Wp2Android.Runner.exe hook <bot token provided by @BotFather> <uri>

Will setup webhook for bot");
                
                return;
            }
            
            if (!File.Exists(args[0]))
            {
                Console.WriteLine($"File {args[0]} not exists");
            }

            using (var content = File.OpenRead(args[0]))
            {
                using (var result = File.OpenWrite(args[0] + ".xml"))
                {
                    new MessageConverter().Convert(content, result);
                    Console.WriteLine($"Written {result.Length} bytes");
                }
            }

            Console.WriteLine($"Saved result in file: {args[0] + ".xml"}");
        }

        private static void HookBot(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("I'm expecting following args: Wp2Android.Runner.exe hook <botKey> <uri>");
            }

            var client = new TelegramBotClient(args[1]);
            client.SetWebhookAsync(args[2]).Wait();
            Console.WriteLine($"Done setting up webhook for bot {client.GetMeAsync().Result.Username} - {args[2]}");
        }
    }
}
