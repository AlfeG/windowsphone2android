#r "Wp2Android.dll" 

using Wp2Android;
using System.Threading;

public static Task<HttpResponseMessage> Run(HttpRequestMessage req, TraceWriter log, CancellationToken token)
{
    return EntryPoint.Run(req, new TraceWriterLogger(log), token);
}

public class TraceWriterLogger : ILogger
{
    private readonly TraceWriter _log;

    public TraceWriterLogger(TraceWriter log)
    {
        _log = log;
    }   

    public void Info(string message, string component = null)
    {
        // _log.Info(message, component);
    }

    public void Error(string message, Exception exception = null)
    {
        _log.Error(message, exception);
    }
}