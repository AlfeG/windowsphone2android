#addin "Cake.Kudu" "https://www.nuget.org/api/v2/"
#tool "KuduSync.NET" "https://www.nuget.org/api/v2/"

var srcProject=  "Wp2Android";
var slnFile = "Wp2Android.sln"; 
var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");
var buildDir = Directory($"./{srcProject}/bin") + Directory(configuration);
var artifactsDir = Directory(".build");

Task("Kudu")
    .Does(() =>
{
    var websitePath = MakeAbsolute(Directory("./Functions"));
    Information("websitePath: " + websitePath);
    var deploymentPath = Kudu.Deployment.Target;

    if (!DirectoryExists(deploymentPath))
    {
        throw new DirectoryNotFoundException(string.Format("Deployment target directory not found {0}", deploymentPath));
    }

    Information("Deploying web from {0} to {1}", websitePath, deploymentPath);
    Kudu.Sync(websitePath);
});

Task("Clean")
    .Does(() =>
{
    CleanDirectory(buildDir);
    CleanDirectory(artifactsDir);
});

Task("Restore-NuGet-Packages")
    .Does(() =>
{
    NuGetRestore($"./{slnFile}");
});

Task("Build")
    .IsDependentOn("Restore-NuGet-Packages")
    .Does(() =>
{
      // Use MSBuild
    MSBuild($"./{slnFile}", settings => settings.SetConfiguration(configuration));  
});

Task("CopyResult") 
    .Does(() => {
        EnsureDirectoryExists("./Functions/Bot/bin");
        CopyDirectory(buildDir, "./Functions/Bot/bin");
});

Task("Default")
    .IsDependentOn("Clean")
    .IsDependentOn("Build")
    .IsDependentOn("CopyResult")
    .IsDependentOn("Kudu");

RunTarget(target);